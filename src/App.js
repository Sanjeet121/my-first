import './App.css';
import React ,{Component} from 'react';
import BlogCard from './BlogCard';

class App extends Component{
   blogObj = {
    title : 'Blog title 1',
    description :'Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar',
  }
  blogArr = [
    {
      title : 'Blog title 1',
      description :'Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar',
    },
    {
      title : 'Blog title 2',
      description :'Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar',
    },
    {
      title : 'Blog title 3',
      description :'Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar',
    },
    {
      title : 'Blog title 4',
      description :'Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar Lorem Ipsum dolar',
    }
  ]
  styleBox =  {
    margin : '20px',
    borderRadius : '5px',
    boxShadow : '0 2px 5px #ccc',
    padding : '16px',
    boxSizing : 'border-box'
  }

  blogCards = this.blogArr.map((items,pos)=>{
    console.log(items);
    return(
      // <div className="BlogCard" key={pos}>
      //   <h3>{items.title}</h3>
      //   <p>{items.description}</p>
      // </div>
      <BlogCard key={pos} title={items.title} description={items.description}/>
    )
  })
onHideBtnClick = ()=>{
  // alert("Button click");
  this.state.showBlog ? this.setState({showBlog : false}) : this.setState({showBlog : true});
  // this.setState({showBlog : false});
}
  state = {
    showBlog : true 
  }

  render(){
    return( 
    <div className="App">
      <button onClick={this.onHideBtnClick}>Hide List</button>
      <br></br>
    {/* <div style={styleBox}>
      <h3>{blogObj.title}</h3>
      <p>{blogObj.description}</p>
    </div>
    <div className="BlogCard">
      <h3>{blogObj.title}</h3>
      <p>{blogObj.description}</p>
    </div>
    {blogCards} */}
  { this.state.showBlog ?  this.blogCards : null}

  </div>);
  }
  
}

export default App;
