import React from 'react';
// import './BlogCard.css';
import classes from './BlogCard.module.css';
const BlogCard = (properties)=>{
    console.log(properties)
    return(
        <div className={classes.BlogCard}>
        <h3>{properties.title}</h3>
        <p>{properties.description}</p>
      </div>
    )
   
}

export default BlogCard;